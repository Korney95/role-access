Роль access
=========

Управление пользователями в ОС Linux.

* Debian/Ububntu
* Centos 6,7,8


Описание
------------

- SSH ключи находятся тут:
`{PLAYBOOK}/files/access/ssh_pubkeys/test.pub`

- Конфигурация пользователей тут:
`{PLAYBOOK}/vars/access/users.yml`

- Для беспарольного `sudo`, надо добвать, например  в `group_vars/all/all.yaml` параметр `access_sudo_nopass == true`

Пример конфига `{PLAYBOOK}/vars/access/users.yml`:
```
---
user_primary_group: all-users
user_groups:
  sudo-users: "%sudo-users ALL=(ALL) {% if access_sudo_nopass %}NOPASSWD: {% endif %}ALL"


users:
  kkorneenkov:
    name: Kirill Korneenkov
    email: kkorneenkov@mail.ru
    password: $6$8ZF6mdyh771hgEaN$/x3Ce.rjxdKOVjI4LuZ4182KOoku/rOQXrA3NIygiVSFPi2xuZRX0vxyCCaSjKB68am.zUZQLsmD7ImYfSWnw1
    groups:
      - sudo-users
    blocked: false
  test:
    name: test Petrovich
    email: test@mail.ru
    password: '{{ vault_access_test_hash }}'
    groups:
      - all-users
    blocked: false
```

Задается главная на всех группа, в данном случае `all-users`.

Далее указываются дополнительные группы, в качестве значения ключа указыватся строка для `/etc/sudoers`  . В данном случае, если где-то указан параметр `access_sudo_nopass == true`, например в `group_vars/all/all.yaml`, то `sudo` для группы `sudo-users` будет без пароля.


